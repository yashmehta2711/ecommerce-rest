<?php

namespace App\Http\Controllers;

use App\Models\Buyer;
use Facade\FlareClient\Api;
use Illuminate\Http\Request;

class BuyersController extends ApiController
{
    public function index()
    {
        $buyers = Buyer::has('transactions')->get();
        return $this->showAll($buyers);
        // return response()->json(['count' => $buyers->count(), 'data' => $buyers], 200);
    }

    public function show(Buyer $buyer)
    {
        // $buyer = Buyer::has('transactions')->findOrFail($id);
        return $this->showOne($buyer);
        // return response()->json(['data' => $buyer], 200);
    }
}
