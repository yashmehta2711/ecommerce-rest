<?php

namespace App\Http\Controllers;

use App\Models\Seller;
use Illuminate\Http\Request;

class SellersController extends ApiController
{
    public function index()
    {
        $sellers = Seller::has('products')->get();
        return $this->showAll($sellers);
        // return response()->json(['count' => $sellers->count(), 'data' => $sellers], 200);
    }
    public function show(Seller $seller)
    {
        // $seller = Seller::has('products')->findOrFail($id);
        return $this->showOne($seller);
        // return response()->json(['data' => $seller], 200);
    }
}
